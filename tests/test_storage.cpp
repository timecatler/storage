#include "../storage/storage.hpp"

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <boost/iostreams/device/mapped_file.hpp>
#include <boost/iostreams/stream.hpp>
namespace io = boost::iostreams;

#include <boost/mpl/list.hpp>
typedef boost::mpl::list<ArrayStorage, MapStorage, BSTStorage> storages;

#include <boost/lexical_cast.hpp>
using boost::lexical_cast;

#include <tuple>
using std::make_tuple;
using std::tuple;
using std::tie;

#include <map>
using std::map;

#include <vector>
using std::vector;

#include <string>
using std::string;
using std::stoll;

#include <limits>
using std::numeric_limits;

#include <random>
using std::random_device;
using std::mt19937;
using std::uniform_int_distribution;

#include <fstream>
using std::ifstream;
using std::ofstream;

#include <sstream>
using std::istringstream;

#include <chrono>
using std::chrono::duration;
using std::chrono::system_clock;
using std::chrono::duration_cast;

// Loads data from path to stop measuring istream extraction times
// Memory intensive
list<tuple<ll, ll>> prepare_data(string const& path) {
    auto file = io::mapped_file_source(path);
    auto stream = io::stream<io::mapped_file_source>(file);
    list<tuple<ll, ll>> res;
    while(not stream.eof()) {
        long long price, amount;
        stream >> price >> amount;
        res.emplace_back(price, amount);
    }
    file.close();
    return res;
}

template <class T>
void realtime_test(list<tuple<ll, ll>> const& data) {
    T storage;
    auto start = system_clock::now();
    auto start_delta = system_clock::now();
    duration<double> delta;

    string row;
    long long counter = 1;
    auto rd = random_device();
    auto gen = mt19937(rd());
    auto distr = uniform_int_distribution<long long>(numeric_limits<long long>::min(),
                                                     numeric_limits<long long>::max());
    for (auto it : data) {
        storage.add(get<0>(it), get<1>(it));

        storage.get_first();
        storage.get_sum_amount();

        if (counter %2 == 0)
            storage.get_amount(distr(gen));

        if (counter %10 == 0)
            storage.topn(10);

        // Used for first dataset. Commented out as it is needless for others.
//        if (counter %10000 == 0) {
//            auto end_delta = system_clock::now();
//            delta = duration<double>(end_delta - start_delta);
//            BOOST_TEST_MESSAGE("Total iterations " << counter <<
//                             ". Time delta is " << delta.count() << "s.");
//            start_delta = end_delta;
//        }
//
//        if (counter %500000 == 0) {
//            BOOST_TEST_MESSAGE("Interrupted after reaching 500000 iterations.");
//            break;
//        }
//
//        if (delta.count() > 20) {
//            BOOST_TEST_MESSAGE("Interrupted after reaching delta > 20s");
//            break;
//        }

        counter++;
    }
    auto end = system_clock::now();
    duration<double> time = end - start;
    BOOST_TEST_MESSAGE("v Time spent is " << time.count() << "s");
}

template <class T>
void efficency_test(list<tuple<ll, ll>> const& data) {
    // Init random number generator
    auto rd = random_device();
    auto gen = mt19937(rd());
    auto distr = uniform_int_distribution<long long>(numeric_limits<long long>::min(),
                                                     numeric_limits<long long>::max());
    // Fill in test storage
    T storage;
    auto start = system_clock::now();
    for (auto it: data) {
        storage.add(get<0>(it), get<1>(it));
    }
    auto end = system_clock::now();
    duration<double> delta = end - start;
    BOOST_TEST_MESSAGE("Time spent on adding " << data.size() << " items from dataset is " << delta.count() << "s.");
    start = end;

    // Test add
    double mean = 0;
    for (int n = 0; n < 10000; n++) {
        ll price = distr(gen);
        ll amount = distr(gen);

        start = system_clock::now();
        storage.add(price, amount);
        end = system_clock::now();

        delta = end - start;
        mean += delta.count()/10000;

        storage.remove(price);
    }
    BOOST_TEST_MESSAGE("Mean time for `add` is " << mean << "s.");

    // Test delete
    mean = 0;
    for (int n = 0; n < 10000; n++) {
        ll price = distr(gen);
        ll amount = distr(gen);

        start = system_clock::now();
        storage.remove(price);
        end = system_clock::now();

        delta = end - start;
        mean += delta.count()/10000;

        storage.add(price, amount);
    }
    BOOST_TEST_MESSAGE("Mean time for `delete` is " << mean << "s.");

    // Test sum
    mean = 0;
    for (int n = 0; n < 10000; n++) {
        start = system_clock::now();
        storage.get_sum_amount();
        end = system_clock::now();

        delta = end - start;
        mean += delta.count()/10000;
    }
    BOOST_TEST_MESSAGE("Mean time for `get_sum_amount` is " << mean << "s.");

    // Test get
    mean = 0;
    for (int n = 0; n < 10000; n++) {
        ll price = distr(gen);

        start = system_clock::now();
        storage.get_amount(price);
        end = system_clock::now();

        delta = end - start;
        mean += delta.count()/10000;
    }
    BOOST_TEST_MESSAGE("Mean time for `get` is " << mean << "s.");

    // Test get_first
    mean = 0;
    for (int n = 0; n < 10000; n++) {
        start = system_clock::now();
        storage.get_first();
        end = system_clock::now();

        delta = end - start;
        mean += delta.count()/10000;
    }
    BOOST_TEST_MESSAGE("Mean time for `get_first` is " << mean << "s.");

    // Test topn
    mean = 0;
    for (int n = 0; n < 10000; n++) {
        unsigned long long number = distr(gen);

        start = system_clock::now();
        storage.topn(number);
        end = system_clock::now();

        delta = end - start;
        mean += delta.count()/10000;
    }
    BOOST_TEST_MESSAGE("Mean time for `topn` is " << mean << "s.");
}

BOOST_AUTO_TEST_SUITE(IStorage_realisation)
    BOOST_AUTO_TEST_SUITE(add_get_first)
        BOOST_AUTO_TEST_CASE_TEMPLATE(add_get_first, TestType, storages) {
            TestType storage;
            storage.add(128, 42);
            BOOST_REQUIRE(*storage.get_first() == make_tuple(128,42));
        }
    BOOST_AUTO_TEST_SUITE_END()
    BOOST_AUTO_TEST_SUITE(basic_delete)
        BOOST_AUTO_TEST_CASE_TEMPLATE(basic_delete, TestType, storages) {
            TestType storage;
            storage.add(128, 42);
            storage.add(67, 23);
            storage.remove(128);
            BOOST_REQUIRE(*storage.get_first() == make_tuple(67, 23));
        }
    BOOST_AUTO_TEST_SUITE_END()
    BOOST_AUTO_TEST_SUITE(get_amount)
        BOOST_AUTO_TEST_CASE_TEMPLATE(get_amount, TestType, storages) {
            TestType storage;
            storage.add(128, 42);
            BOOST_REQUIRE(*storage.get_amount(128) == 42);
        }
    BOOST_AUTO_TEST_SUITE_END()
    BOOST_AUTO_TEST_SUITE(get_nonexistent_amount)
        BOOST_AUTO_TEST_CASE_TEMPLATE(get_nonexistent_amount, TestType, storages) {
            TestType storage;
            storage.add(128, 42);
            auto res = storage.get_amount(23);
            BOOST_REQUIRE(not res.has_value());
        }
    BOOST_AUTO_TEST_SUITE_END()
    BOOST_AUTO_TEST_SUITE(add_same_price)
        BOOST_AUTO_TEST_CASE_TEMPLATE(add_same_price, TestType, storages) {
            TestType storage;
            storage.add(128, 42);
            storage.add(128, 23);
            BOOST_REQUIRE(*storage.get_amount(128) == 23);
        }
    BOOST_AUTO_TEST_SUITE_END()
    BOOST_AUTO_TEST_SUITE(sum_amounts)
        BOOST_AUTO_TEST_CASE_TEMPLATE(sum_amounts, TestType, storages) {
            TestType storage;
            storage.add(128, 42);
            storage.add(67, 23);
            BOOST_REQUIRE(storage.get_sum_amount() == 42 + 23);
        }
    BOOST_AUTO_TEST_SUITE_END()
    BOOST_AUTO_TEST_SUITE(topn)
        BOOST_AUTO_TEST_SUITE(top0)
            BOOST_AUTO_TEST_CASE_TEMPLATE(top0, TestType, storages) {
                TestType storage;
                storage.add(128, 42);
                auto res = storage.topn(0);
                BOOST_REQUIRE(res.empty());
            }
        BOOST_AUTO_TEST_SUITE_END()
        BOOST_AUTO_TEST_SUITE(top1)
            BOOST_AUTO_TEST_CASE_TEMPLATE(top1, TestType, storages) {
                TestType storage;
                storage.add(128, 42);
                auto res = storage.topn(1).front();
                BOOST_REQUIRE(res == storage.get_first());
            }
        BOOST_AUTO_TEST_SUITE_END()
        BOOST_AUTO_TEST_SUITE(top2)
            BOOST_AUTO_TEST_CASE_TEMPLATE(top2, TestType, storages) {
                TestType storage;
                storage.add(128, 42);
                storage.add(67, 23);
                auto res = storage.topn(2);
                auto it = res.begin();
                BOOST_REQUIRE(*it == make_tuple(67, 23));
                it++;
                BOOST_REQUIRE(*it == make_tuple(128, 42));
            }
        BOOST_AUTO_TEST_SUITE_END()
        BOOST_AUTO_TEST_SUITE(top3)
            BOOST_AUTO_TEST_CASE_TEMPLATE(top3, TestType, storages) {
                TestType storage;
                storage.add(128, 42);
                auto top2 = storage.topn(2);
                auto top3 = storage.topn(3);
                BOOST_REQUIRE(top2 == top3);
            }
        BOOST_AUTO_TEST_SUITE_END()
    BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(large_amounts)
    BOOST_AUTO_TEST_CASE_TEMPLATE(large_amounts, TestType, storages) {
        TestType storage;
        auto rd = random_device();
        auto gen = mt19937(rd());
        auto distr = uniform_int_distribution<long long>(numeric_limits<long long>::min(),
                                                         numeric_limits<long long>::max());
        // prepare data
        map<long long, long long> control_storage;
        auto num_vals = 10000;
        for (int n = 0; n < num_vals; n++) {
            auto price = distr(gen);
            auto amount = distr(gen);
            storage.add(price, amount);
            control_storage[price] = amount;
        }

        // convert data to vector of tuples (easy to compare)
        list<tuple<long long, long long>> control_res;
        for (auto it : control_storage) {
            control_res.emplace_back(it);
        }
        auto res = storage.topn(num_vals);
        BOOST_REQUIRE(res == control_res);
    }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(realtime)
    vector<string> datasets = {"./data/dataset1.log",
                               "./data/dataset2.log",
                               "./data/dataset3.log",
                               "./data/dataset4.log"};
    // First and second dataset are very time and memory consuming so I had to run them separately.
    // First dataset in fact takes so much time so I had to change testing strategy (break realtime test on condition)

//    BOOST_AUTO_TEST_SUITE(dataset1)
//        auto data = prepare_data(datasets[0]);
//        BOOST_AUTO_TEST_CASE_TEMPLATE(realtime, TestType, storages) {
//            realtime_test<TestType>(data);
//        }
//    BOOST_AUTO_TEST_SUITE_END()

//    BOOST_AUTO_TEST_SUITE(dataset2)
//        auto data = prepare_data(datasets[1]);
//        BOOST_AUTO_TEST_CASE_TEMPLATE(realtime, TestType, storages) {
//            realtime_test<TestType>(data);
//        }
//    BOOST_AUTO_TEST_SUITE_END()

    BOOST_AUTO_TEST_SUITE(dataset3)
        auto data = prepare_data(datasets[2]);
        BOOST_AUTO_TEST_CASE_TEMPLATE(realtime, TestType, storages) {
            realtime_test<TestType>(data);
        }
    BOOST_AUTO_TEST_SUITE_END()

    BOOST_AUTO_TEST_SUITE(dataset4)
        auto data = prepare_data(datasets[3]);
        BOOST_AUTO_TEST_CASE_TEMPLATE(realtime, TestType, storages) {
            realtime_test<TestType>(data);
        }
    BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(efficency)
    vector<string> datasets = {"./data/dataset1.log",
                           "./data/dataset2.log",
                           "./data/dataset3.log",
                           "./data/dataset4.log"};
    // Takes too much time
//    BOOST_AUTO_TEST_SUITE(dataset1)
//        auto data = prepare_data(datasets[0]);
//        BOOST_AUTO_TEST_CASE_TEMPLATE(realtime, TestType, storages) { efficency_test<TestType>(data); }
//    BOOST_AUTO_TEST_SUITE_END()

    BOOST_AUTO_TEST_SUITE(dataset2)
        auto data = prepare_data(datasets[1]);
        BOOST_AUTO_TEST_CASE_TEMPLATE(realtime, TestType, storages) { efficency_test<TestType>(data); }
    BOOST_AUTO_TEST_SUITE_END()

    BOOST_AUTO_TEST_SUITE(dataset3)
        auto data = prepare_data(datasets[2]);
        BOOST_AUTO_TEST_CASE_TEMPLATE(realtime, TestType, storages) { efficency_test<TestType>(data); }
    BOOST_AUTO_TEST_SUITE_END()

    BOOST_AUTO_TEST_SUITE(dataset4)
        auto data = prepare_data(datasets[3]);
        BOOST_AUTO_TEST_CASE_TEMPLATE(realtime, TestType, storages) { efficency_test<TestType>(data); }
    BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()
