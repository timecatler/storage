#include "BSTStorage.hpp"

void BSTStorage::add(ll price, ll amount) {
    if (amount == 0) {
        data.remove(price);
    } else {
        data.add(price, amount);
    }
}

void BSTStorage::remove(ll price) {
    data.remove(price);
}

unsigned long long int BSTStorage::get_sum_amount() {
    unsigned long long res = 0;
    // Fold with sum
    data.traverse_infix([&](ll const price, ll const amount) { res+= amount; });
    return res;
}

optional<ll> BSTStorage::get_amount(ll price) {
    auto res = data.get(price);
    if (res.has_value())
        return get<1>(res.value());
    else
        return nullopt;
}

optional<tuple<ll, ll>> BSTStorage::get_first() {
    if (data.empty())
        return nullopt;
    else
        return data.get_first();
}

list<tuple<ll, ll>> BSTStorage::topn(unsigned long long n) {
    list<tuple<ll, ll>> res;
    if (n == 0)
        return res;
    if (n > data.get_size()) {
        // Add every element to res
        data.traverse_infix([&](ll const price, ll const amount) { res.emplace_back(price, amount); });
        return res;
    }
    // 0 < n < data.size()
    // Add n elements to res
    data.traverse_infix([&](ll const price, ll const amount) {
        if (n <= 0)
            return;
        res.emplace_back(price, amount);
        n--;
    });
    return res;
}
