#ifndef STORAGE_BSTREE_HPP
#define STORAGE_BSTREE_HPP

#include <memory>
using std::shared_ptr;
using std::make_shared;

#include <tuple>
using std::tuple;
using std::make_tuple;
using std::tie;

#include <list>
using std::list;

#include <forward_list>
using std::forward_list;

#include <optional>
using std::nullopt;
using std::optional;

#include <algorithm>
using std::max;

#include <functional>
using std::function;

/**
 * Binary search tree (specifically AVL tree)
 * @tparam Key_t Key type
 * @tparam Value_t Value type
 */
template <typename Key_t, typename Value_t>
class BSTree {
private:
    struct Node;
    using Node_ptr_t = shared_ptr<Node>;

    Node_ptr_t root;
    long long size;

    struct Node {
        Key_t key;
        Value_t value;
        long long height;
        shared_ptr<Node> left,
                         right;
        Node() = default;
        Node(Key_t key, Value_t value) : key(key),
                                         value(value),
                                         height(1),
                                         left(nullptr),
                                         right(nullptr) {}
    };

    /**
     * Returns height for node
     * @param node Target node
     * @return Height of node (0 if node is nullptr)
     */
    static long long get_height(Node_ptr_t node) {
        if (node == nullptr)
            return 0;
        else
            return node->height;
    }

    /**
     * Returns balance factor for node
     * @param node Target node
     * @return
     */
    static long long get_balance(Node_ptr_t node) {
        if (node == nullptr)
            return 0;
        else
            return get_height(node->left) - get_height(node->right);
    }

    /**
     * Perform right rotation on node
     * @param node Target node
     * @return Target node replacement
     */
    static Node_ptr_t rotate_right(Node_ptr_t node) {
        Node_ptr_t res = node->left;
        node->left = res->right;
        res->right = node;
        node->height = max(get_height(node->left), get_height(node->right)) + 1;
        res->height = max(get_height(res->left), node->height) + 1;
        return res;
    }

    /**
     * Perform left rotation on node
     * @param node Target node
     * @return Target node replacement
     */
    static Node_ptr_t rotate_left(Node_ptr_t node) {
        Node_ptr_t res = node->right;
        node->right = res->left;
        res->left = node;
        node->height = max(get_height(node->right), get_height(node->left)) + 1;
        res->height = max(get_height(res->right), node->height) + 1;
        return res;
    }

    /**
     * Perform left to right rotation on node
     * @param node Target node
     * @return Target node replacement
     */
    static Node_ptr_t rotate_left_right(Node_ptr_t node) {
        node->left = rotate_left(node->left);
        return rotate_right(node);
    }

    /**
     * Perform right to left rotation on node
     * @param node Target node
     * @return Target node replacement
     */
    static Node_ptr_t rotate_right_left(Node_ptr_t node) {
        node->right = rotate_right(node->right);
        return rotate_left(node);
    }

    /**
     * Find node in subtree with minimum key
     * @param start Node to start searching from
     * @return Node with minimum key
     */
    static Node_ptr_t find_min(Node_ptr_t start) {
        if (start == nullptr)
            return nullptr;
        while (start->left != nullptr)
            start = start->left;
        return start;
    }

    /**
     * Find node in subtree with maximum key
     * @param start Node to start searching from
     * @return Node with maximum key
     */
    static Node_ptr_t find_max(Node_ptr_t start) {
        if (start == nullptr)
            return nullptr;
        while (start->right != nullptr)
            start = start->right;
        return start;
    }

    /**
     * Recursively remove node from subtree
     * @param key Key of the node to remove
     * @param node Target subtree root
     * @return Replacement for target subtree root
     */
    static Node_ptr_t remove_recursive(Key_t key, Node_ptr_t node) {
        if (node == nullptr)
            return nullptr;
        if (key < node->key)
            node->left = remove_recursive(key, node->left);
        else if (key > node->key)
            node->right = remove_recursive(key, node->right);
        else {
            if (node->left == nullptr) node = node->right;
            else if (node->right == nullptr) node = node->left;
            else {
                auto tmp = find_min(node->right);
                node->value = tmp->value;
                node->key = tmp->key;
                node->right = remove_recursive(node->key, node->right);
            }
            if (node == nullptr) return nullptr;
        }

        node->height = max(get_height(node->left), get_height(node->right)) + 1;

        // Unbalanced cases
        auto balance = get_balance(node);
        if (balance < -1) {
            if (get_balance(node->right) <= 0)
                return rotate_left(node);
            else
                return rotate_right_left(node);
        } else if (balance > 1) {
            if (get_balance(node->left) >= 0)
                return rotate_right(node);
            else
                return rotate_left_right(node);
        }

        return node;
    }

    /**
     * Recursively insert node to subtree (replace value if key exists)
     * @param key Key of the node to insert
     * @param value Value of the node to insert
     * @param node Target subtree root
     * @return Replacement for target subtree root
     */
    Node_ptr_t insert_recursive(Key_t key, Value_t value, Node_ptr_t node) {
        if (node == nullptr) {
            node = make_shared<Node>(key, value);
        } else if (key < node->key) {
            node->left = insert_recursive(key, value, node->left);
        } else if (key > node->key) {
            node->right = insert_recursive(key, value, node->right);
        } else {
            node->value = value;
            size--; //< was incremented in add
        }

        node->height = max(get_height(node->left), get_height(node->right)) + 1;
        // Rebalancing
        auto balance = get_balance(node);
        if (balance < -1) {
            if (key > node->right->key)
                return rotate_left(node);
            else if (key < node->right->key)
                return rotate_right_left(node);
        } else if (balance > 1) {
            if (key < node->left->key)
                return rotate_right(node);
            else if (key > node->left->key)
                return rotate_left_right(node);
        }

        return node;
    }

    /**
     * Find node with set key
     * @param key Key of the target node
     * @return Target node or nullptr if it doesn't exist
     */
    Node_ptr_t find(Key_t key) {
        Node_ptr_t res = root;
        while (res != nullptr && res->key != key) {
            if (key < res->key)
                res = res->left;
            else
                res = res->right;
        }
        return res;
    }

    /**
     * Traverse subtree in infix order applying consumer function to every element
     * @param consume Consumer function
     * @param node Target subtree
     */
    static void traverse_infix(function<void(Key_t, Value_t)> consume, Node_ptr_t node) {
        if (node == nullptr) return;
        traverse_infix(consume, node->left);
        consume(node->key, node->value);
        traverse_infix(consume, node->right);
    }
public:
    BSTree() : root(nullptr), size(0) {}

    /**
     * Remove node from tree
     * @param key Key of node to remove
     */
    void remove(Key_t key) {
        if (size == 0) {
            return;
        } else {
            root = remove_recursive(key, root);
            size--;
        }
    }

    /**
     * Add node to tree (replace if key exists)
     * @param key Key of node to add
     * @param value Value of node to add
     */
    void add(Key_t key, Value_t value) {
        root = insert_recursive(key, value, root);
        size++;
    }

    /**
     * Return node with set key
     * @param key Key of requested node
     * @return Tuple of key and value of node or nullopt if it doesn't exist
     */
    optional<tuple<Key_t, Value_t>> get(Key_t key) {
        auto tmp = find(key);
        if (tmp == nullptr)
            return nullopt;
        else
            return make_tuple(key, tmp->value);
    }

    /**
     * Get amount of tree nodes
     * @return Amount of tree nodes
     */
    long long get_size() { return size; }

    /**
     * Find out if tree is empty
     * @return True if it is empty, false otherwise
     */
    bool empty() { return size == 0; }

    /**
     * Traverse tree in infix order applying consumer function to every element
     * @param consume Consumer function
     */
    void traverse_infix(function<void(Key_t, Value_t)> consume) {
        traverse_infix(consume, root);
    }

    /**
     * Get tree element with minimal key
     * @return
     */
    optional<tuple<Key_t, Value_t>> get_first() {
        auto res = find_min(root);
        if (res == nullptr)
            return nullopt;
        else
            return make_tuple(res->key, res->value);
    };
};


#endif //STORAGE_BSTREE_HPP
