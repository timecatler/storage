#ifndef STORAGE_MAPSTORAGE_HPP
#define STORAGE_MAPSTORAGE_HPP

#include "IStorage.hpp"

#include <map>
using std::map;

#include <numeric>
using std::accumulate;

#include <tuple>
using std::get;

#include <optional>
using std::make_optional;
using std::nullopt;

class MapStorage : public IStorage {
private:
    map<ll, ll> data;
public:
    void add(ll price, ll amount) override;

    void remove(ll price) override;

    unsigned long long int get_sum_amount() override;

    optional<ll> get_amount(ll price) override;

    optional<tuple<ll, ll>> get_first() override;

    list<tuple<ll, ll>> topn(unsigned long long n) override;
};


#endif //STORAGE_MAPSTORAGE_HPP
