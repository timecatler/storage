#ifndef STORAGE_BSTSTORAGE_HPP
#define STORAGE_BSTSTORAGE_HPP

#include "IStorage.hpp"
#include "containers/BsTree.hpp"

#include <tuple>
using std::get;

#include <numeric>
using std::accumulate;

class BSTStorage : public IStorage {
private:
    BSTree<ll, ll> data;
public:
    void add(ll price, ll amount) override;

    void remove(ll price) override;

    unsigned long long int get_sum_amount() override;

    optional<ll> get_amount(ll price) override;

    optional<tuple<ll, ll>> get_first() override;

    list<tuple<ll, ll>> topn(unsigned long long n) override;
};


#endif //STORAGE_BSTSTORAGE_HPP
