#ifndef STORAGE_STORAGE_HPP
#define STORAGE_STORAGE_HPP

#include "IStorage.hpp"
#include "ArrayStorage.hpp"
#include "MapStorage.hpp"
#include "BSTStorage.hpp"

#endif //STORAGE_STORAGE_HPP
