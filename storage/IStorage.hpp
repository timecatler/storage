#ifndef STORAGE_ISTORAGE_HPP
#define STORAGE_ISTORAGE_HPP

#include <tuple>
using std::tuple;

#include <optional>
using std::optional;

#include <list>
using std::list;

using ll = long long;

/**
 * Interface for records storage
 */
class IStorage {
public:
    virtual ~IStorage() = default;
    /**
     * Adds record (price, amount) to the storage.
     * @param price Price for the record (unique).
     * @param amount Amount for the record.
     */
    virtual void add(ll price, ll amount) = 0;
    /**
     * Removes record with set price from the storage.
     * Does nothing if there's no record with this price.
     * @param price Price to identify record for removal.
     */
    virtual void remove(ll price) = 0;
    /**
     * Accumulates all amounts from records in storage.
     * @return Sum of all amounts in storage.
     */
    virtual unsigned long long get_sum_amount() = 0; //< sum of amounts can be larger than long long, but can't be <0
    /**
     * Returns amount from record with set price.
     * @param price Price to identify the record.
     * @return Amount for the record if there is record with set price.
     *         Empty optional otherwise
     */
    virtual optional<ll> get_amount(ll price) = 0;
    /**
     * Returns first record in storage.
     * @return First record in storage if there is 1+ record.
     *         Empty optional otherwise.
     */
    virtual optional<tuple<ll, ll>> get_first() = 0;
    /**
     * Returns top n records from storage (sorted ascending).
     * @param n Amount of records to return.
     * @return List with n records if n <= amount of stored records.
     *         List with all stored records if n > amount of stored records.
     */
    virtual list<tuple<ll, ll>> topn(unsigned long long n) = 0;
};

#endif //STORAGE_ISTORAGE_HPP
