#include "ArrayStorage.hpp"

auto ArrayStorage::find_by_price(ll price) {
    auto compare = [](tuple<ll, ll> record, ll price_val) -> bool { return get<0>(record) < price_val; };
    return lower_bound(data.begin(), data.end(), price, compare);
}

void ArrayStorage::add(ll price, ll amount) {
    if (amount == 0) {
        return remove(price);
    }
    // Find first record with price > price argument
    auto pos = find_by_price(price);
    // If there is one
    if (pos != data.end()) {
        // If it has equal price
        if (get<0>(*pos) == price) {
            // Replace amount
            get<1>(*pos) = amount;
            return;
        }
    }
    // Insert in end or after pos
    data.insert(pos, make_tuple(price, amount));
}

void ArrayStorage::remove(ll price) {
    auto pos = find_by_price(price);
    if (pos != data.end())
        data.erase(pos);
}

unsigned long long int ArrayStorage::get_sum_amount() {
    unsigned long long res = 0;
    res = accumulate(data.begin(), data.end(), res,
                     [](auto acc, auto node) -> decltype(acc) { return acc + get<1>(node); });
    return res;
}

optional<ll> ArrayStorage::get_amount(ll price) {
    auto pos = find_by_price(price);
    if (pos == data.end()
     || get<0>(*pos) != price)
        return nullopt;
    else
        return get<1>(*pos);
}

optional<tuple<ll, ll>> ArrayStorage::get_first() {
    if (data.empty())
        return nullopt;
    else
        return data.front();
}

list<tuple<ll, ll>> ArrayStorage::topn(unsigned long long n) {
    list<tuple<ll, ll>> res;
    if (n == 0)
        return res;
    if (n > data.size()) {
        for (auto it: data)
            res.push_back(it);
        return res;
    }
    // 0 < n < data.size()
    for (auto it : data) {
        if (n <= 0)
            break;
        res.push_back(it);
        n--;
    }
    return res;
}
