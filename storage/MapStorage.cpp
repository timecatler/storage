#include "MapStorage.hpp"

void MapStorage::add(ll price, ll amount) {
    if (amount == 0) {
        data.erase(price);
    } else {
        data[price] = amount;
    }
}

void MapStorage::remove(ll price) {
    data.erase(price);
}

unsigned long long int MapStorage::get_sum_amount() {
    unsigned long long res = 0;
    res = accumulate(data.begin(), data.end(), res,
                     [](auto acc, auto node) -> decltype(acc) {return acc + get<1>(node);});
    return res;
}

optional<ll> MapStorage::get_amount(ll price) {
    auto pos = data.find(price);
    if (pos == data.end()) //< Nothing is found
        return nullopt;
    else
        return get<1>(*pos); //< amount at pos.
}

optional<tuple<ll, ll>> MapStorage::get_first() {
    if (data.size() == 0)
        return nullopt;
    else
        return *data.begin();
}

list<tuple<ll, ll>> MapStorage::topn(unsigned long long n) {
    list<tuple<ll, ll>> res;
    if (n == 0)
        return res;
    if (n > data.size()) {
        for (auto it: data)
            res.push_back(it);
        return res;
    }
    // 0 < n < data.size()
    for (auto it : data) {
        if (n <= 0)
            break;
        res.push_back(it);
        n--;
    }
    return res;
}
