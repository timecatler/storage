#ifndef STORAGE_ARRAYSTORAGE_HPP
#define STORAGE_ARRAYSTORAGE_HPP

#include "IStorage.hpp"

#include <vector>
using std::vector;

#include <tuple>
using std::make_tuple;
using std::get;

#include <algorithm>
using std::lower_bound; //< Binary search

#include <numeric>
using std::accumulate;

#include <optional>
using std::nullopt;

class ArrayStorage : public IStorage {
private:
    vector<tuple<ll, ll>> data;
    auto find_by_price(ll price);
public:
    void add(ll price, ll amount) override;

    void remove(ll price) override;

    unsigned long long int get_sum_amount() override;

    optional<ll> get_amount(ll price) override;

    optional<tuple<ll, ll>> get_first() override;

    list<tuple<ll, ll>> topn(unsigned long long n) override;
};

#endif //STORAGE_ARRAYSTORAGE_HPP
